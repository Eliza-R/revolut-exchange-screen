export const CHANGE_SOURCE = 'CHANGE_SOURCE';
export const UPDATE_RATES = 'UPDATE_RATES';
export const CURRENCY_CHANGED = 'CURRENCY_CHANGED';
export const ERROR = 'ERROR';
export const EXCHANGE = 'EXCHANGE';

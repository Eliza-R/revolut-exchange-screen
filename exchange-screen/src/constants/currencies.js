export default {
  GBP: { name: 'GBP', sign: '£' },
  EUR: { name: 'EUR', sign: '€' },
  USD: { name: 'USD', sign: '$' },
  CHF: { name: 'CHF', sign: '₣' },
  CNY: { name: 'CNY', sign: '¥' },
  COP: { name: 'COP', sign: '$' },
};

import React, { PureComponent } from 'react';
import pt from 'prop-types';
import getCurrencySign from '../../helpers/getCurrencySign';
import s from './Currency.module.css';

class Currency extends PureComponent {
  static propTypes = {
    currencyName: pt.string.isRequired,
    availableMoney: pt.number,
    rate: pt.string,
    value: pt.node.isRequired,
    bgColor: pt.string,
  };

  static defaultProps = {
    rate: '',
    bgColor: '#385fc7',
    availableMoney: undefined,
  }

  render() {
    const {
      currencyName,
      availableMoney,
      value,
      rate,
      bgColor,
    } = this.props;

    return (
      <div
        className={s.wrapper}
        style={{ backgroundColor: bgColor }}
      >
        <div className={s.mainValues}>
          <span>{currencyName}</span>
          {value}
        </div>
        <div className={s.additionalInfo}>
          <span>You have {getCurrencySign(currencyName)}{availableMoney}</span>
          {rate ? <span>{rate}</span> : null}
        </div>
      </div>
    );
  }
}

export default Currency;

import React from 'react';
import { shallow } from 'enzyme';
import Currency from '../Currency';

const commonProps = {
  currencyName: 'EUR',
  availableMoney: '16.12',
  value: 34,
};

it('renders without crashing', () => {
  shallow(<Currency {...commonProps} />);
});

it('renders available amount', () => {
  const wrapper = shallow(<Currency
    {...commonProps}
  />);

  expect(wrapper.find('.additionalInfo span').first().text()).toBe('You have €16.12');
});

it('does not render rate if no rate', () => {
  const wrapper = shallow(<Currency
    {...commonProps}
  />);

  expect(wrapper.find('.additionalInfo span').at(1)).toBe(undefined);
});

it.only('renders rate if rate is present', () => {
  const rate = '1£ = €0.74';
  const wrapper = shallow(<Currency
    {...commonProps}
    rate={rate}
  />);
  expect(wrapper.find('.additionalInfo span').at(1).text()).toBe(rate);
});

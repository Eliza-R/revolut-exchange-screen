import React, { PureComponent } from 'react';
import pt from 'prop-types';
import memoize from 'memoize-one';
import formatMoneyValue from '../../helpers/formatMoneyValue';
import getRateInfo from '../../helpers/getRateInfo';
import Currency from '../Currency/Currency';
import s from './CurrencyTo.module.css';

const SIGN = '+';
const getNodeValue = memoize(value => <span className={s.nodeValue}>{value}</span>);

class CurrencyTo extends PureComponent {
  static propTypes = {
    currencyName: pt.string.isRequired,
    sourceCurrencyName: pt.string.isRequired,
    availableMoney: pt.number,
    rate: pt.number,
    value: pt.number,
  };

  static defaultProps = {
    value: undefined,
    rate: undefined,
    availableMoney: undefined,
  }

  render() {
    const {
      value,
      sourceCurrencyName,
      currencyName,
      rate,
    } = this.props;

    const nodeValue = getNodeValue(formatMoneyValue(SIGN, value));
    return (
      <Currency
        {...this.props}
        value={nodeValue}
        rate={getRateInfo(sourceCurrencyName, currencyName, rate)}
      />
    );
  }
}

export default CurrencyTo;

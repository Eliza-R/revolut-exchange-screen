import React from 'react';
import { mount } from 'enzyme';
import CurrencyTo from '../CurrencyTo';

const commonProps = {
  currencyName: 'EUR',
  sourceCurrencyName: 'GBP',
  availableMoney: '16.12',
  value: 34,
  rate: 0.74,
};

const wrapper = mount(<CurrencyTo {...commonProps} />);

it('renders as read-only', () => {
  expect(wrapper.find('input').length).toBe(0);
  expect(wrapper.find('.mainValues span').at(1).text()).toBe('+34');
});

it('renders with correct values', () => {
  wrapper.setProps({ value: 0 });
  expect(wrapper.find('.mainValues span').at(1).text()).toBe('+0');

  wrapper.setProps({ value: undefined });
  expect(wrapper.find('.mainValues span').at(1).text()).toBe('');
});

it('renders with correct rate info', () => {
  expect(wrapper.find('Currency').props().rate).toBe('€1 = £0.74');
});

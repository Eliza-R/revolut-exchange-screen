import React, { PureComponent } from 'react';
import pt from 'prop-types';
import formatMoneyValue from '../../helpers/formatMoneyValue';

import Currency from '../Currency/Currency';
import Input from '../Input/Input';

const SIGN = '-';
class CurrencyFrom extends PureComponent {
  static propTypes = {
    currencyName: pt.string.isRequired,
    availableMoney: pt.number,
    value: pt.string,
    onChange: pt.func.isRequired,
  };

  static defaultProps = {
    value: '',
    availableMoney: undefined,
  }

  handleChange = (value) => {
    const { onChange } = this.props;
    onChange(value.replace(SIGN, ''));
  }

  render() {
    const { value } = this.props;
    const valueNode = <Input value={formatMoneyValue(SIGN, value)} onChange={this.handleChange} />;
    return <Currency {...this.props} value={valueNode} bgColor="#376EF1" />;
  }
}

export default CurrencyFrom;

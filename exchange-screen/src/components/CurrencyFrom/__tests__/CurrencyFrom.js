import React from 'react';
import { mount } from 'enzyme';
import CurrencyFrom from '../CurrencyFrom';

const commonProps = {
  currencyName: 'EUR',
  availableMoney: '16.12',
  value: 34,
};

const wrapper = mount(<CurrencyFrom
  {...commonProps}
  input
/>);

it('renders with input', () => {
  expect(wrapper.find('input').length).toBe(1);
});

it('renders with correct values', () => {
  expect(wrapper.find('input').props().value).toBe('-34');

  wrapper.setProps({ value: 0 });
  expect(wrapper.find('input').props().value).toBe('-0');

  wrapper.setProps({ value: undefined });
  expect(wrapper.find('input').props().value).toBe('');

  wrapper.setProps({ value: '-71' });
  expect(wrapper.find('input').props().value).toBe('-71');
});

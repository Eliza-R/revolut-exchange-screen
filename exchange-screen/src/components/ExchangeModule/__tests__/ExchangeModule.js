import React from 'react';

import { getCurrencies } from '../ExchangeModule';

it('getCurrencies provides correct first currency', () => {
  const Component = () => <div />;
  const props = { availableMoney: '58.33', value: '6' };
  expect(getCurrencies('EUR', Component, props)[0].props).toEqual({ ...props, currencyName: 'EUR' });
});

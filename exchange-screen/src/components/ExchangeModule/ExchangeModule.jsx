import React, { PureComponent } from 'react';
import {
  compose, filter, complement, values,
} from 'ramda';
import memoize from 'memoize-one';
import getTargetAmount from '../../helpers/getTargetAmount';
import { isValue } from '../../helpers/utils';
import getRateInfo from '../../helpers/getRateInfo';
import { DEFAULT_CURRENCY_FROM, DEFAULT_CURRENCY_TO } from '../../constants/defaults';
import s from './ExchangeModule.module.css';
import CurrencyFrom from '../CurrencyFrom/CurrencyFrom';
import CurrencyTo from '../CurrencyTo/CurrencyTo';
import Carousel from '../Carousel/Carousel';
import currencies from '../../constants/currencies';

export const getCurrencies = (defaultCurrencyName, Component, props) => {
  const isDefault = currency => currency.name === defaultCurrencyName;
  const defaultCurrency = compose(values, filter(isDefault))(currencies);
  const rest = compose(values, filter(complement(isDefault)))(currencies);
  const orderedCurrencies = [...defaultCurrency, ...rest];

  return orderedCurrencies.map(currency => (
    <Component
      {...props}
      currencyName={currency.name}
      key={currency.name}
    />
  ));
};

const getCurrenciesFrom = memoize(
  (onChange, availableMoney, value) => getCurrencies(DEFAULT_CURRENCY_FROM, CurrencyFrom, {
    onChange,
    availableMoney,
    value,
  }),
);
const getCurrenciesTo = memoize(
  (availableMoney, value, rate, sourceCurrencyName) => getCurrencies(DEFAULT_CURRENCY_TO,
    CurrencyTo, {
      availableMoney,
      value,
      rate,
      sourceCurrencyName,
    }),
);

const convertCurrencies = (ratesLoaded, ...args) => {
  if (!ratesLoaded) {
    return undefined;
  }
  return getTargetAmount(...args);
};
class ExchangeModule extends PureComponent {
  state = {}

  handleCurrencyFromChange = (...args) => {
    const {
      changeCurrency,
    } = this.props;

    this.handleCurrencyChange();
    changeCurrency(...args, true);
  }

  handleCurrencyToChange = (...args) => {
    const {
      changeCurrency,
    } = this.props;

    this.handleCurrencyChange();
    changeCurrency(...args, false);
  }

  handleSourceChange = (sourceValue) => {
    const {
      changeSource,
    } = this.props;

    this.updateTargetValue({ sourceValue });
    changeSource(sourceValue);
  }

  updateTargetValue = ({ sourceValue } = this.props) => {
    const {
      currencyFrom,
      currencyTo,
      rates,
      ratesLoaded,
    } = this.props;

    const targetValue = isValue(sourceValue) && ratesLoaded
      ? getTargetAmount(+sourceValue, currencyFrom, currencyTo, rates) : undefined;

    this.setState({
      targetValue,
    });
  }

  handleExchange = () => {
    const {
      sourceValue,
      exchange,
      currencyFrom,
      currencyTo,
    } = this.props;
    const { targetValue } = this.state;
    exchange({
      sourceValue, targetValue, currencyFrom, currencyTo,
    });
  }

  isValid = () => {
    const {
      sourceValue,
      currencyFrom,
      currencyTo,
      accounts,
    } = this.props;

    const notEnoughMoney = sourceValue > accounts[currencyFrom];
    const noValue = !sourceValue || sourceValue === '0';
    const sameCurrency = currencyFrom === currencyTo;

    return notEnoughMoney || noValue || sameCurrency;
  }

  handleCurrencyChange() {
    const {
      sourceValue,
    } = this.props;

    this.updateTargetValue({ sourceValue });
  }


  render() {
    const {
      sourceValue,
      currencyFrom,
      currencyTo,
      rates,
      ratesLoaded,
      indexFrom,
      indexTo,
      accounts,
    } = this.props;
    const { targetValue } = this.state;

    const rate = convertCurrencies(ratesLoaded, 1, currencyTo, currencyFrom, rates);
    const revertedRate = convertCurrencies(ratesLoaded, 1, currencyFrom, currencyTo, rates);
    const availableMoneySource = accounts[currencyFrom];
    const availableMoneyTarget = accounts[currencyTo];

    return (
      <div className={s.wrapper}>
        <div className={s.controlPanel}>
          <button type="button">Cancel</button>
          <div className={s.rate}>{getRateInfo(currencyFrom, currencyTo, revertedRate)}</div>
          <button
            type="button"
            onClick={this.handleExchange}
            disabled={this.isValid()}
          >
            Exchange
          </button>
        </div>
        <div className={s.currencyFromWrapper}>
          <div className={s.currencyFrom}>
            <Carousel
              onChange={this.handleCurrencyFromChange}
              index={indexFrom}
            >
              {getCurrenciesFrom(this.handleSourceChange, availableMoneySource, sourceValue)}
            </Carousel>
          </div>
        </div>
        <div className={s.currencyToWrapper}>
          <div className={s.currencyTo}>
            <Carousel
              onChange={this.handleCurrencyToChange}
              index={indexTo}
            >
              {getCurrenciesTo(availableMoneyTarget, targetValue, rate, currencyFrom)}
            </Carousel>
          </div>
        </div>
      </div>
    );
  }
}

export default ExchangeModule;

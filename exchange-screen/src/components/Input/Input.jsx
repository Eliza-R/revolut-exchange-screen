import React, { PureComponent } from 'react';
import pt from 'prop-types';
import { noop } from '../../helpers/utils';
import s from './Input.module.css';

const DECIMALS_MATCHER = /[.|,]+/g;
const SYMBOLS_TO_SKIP = /[^\d,+-/*().]/g;
const ZEROS_IN_BEGINNING = /^-0+/g;

export const formatNumber = (value) => {
  const valWithDots = value.replace(DECIMALS_MATCHER, '.').replace(SYMBOLS_TO_SKIP, '').replace(ZEROS_IN_BEGINNING, '-0');
  const decimalParts = valWithDots.split('.', 2);
  const isInt = decimalParts.length === 1;

  if (isInt) {
    return decimalParts[0];
  }

  return `${decimalParts[0]}.${decimalParts[1].slice(0, 2).replace('.', '')}`;
};

class Input extends PureComponent {
    state = {
      value: '',
    }

    static getDerivedStateFromProps(props, state) {
      if (props.value !== state.value) {
        return { value: props.value };
      }

      return null;
    }

    handleChange = ({ target }) => {
      const { onChange } = this.props;
      const { value } = this.state;

      const formattedNumber = formatNumber(target.value);
      if (formattedNumber !== value) {
        onChange(formattedNumber);
      }
      this.setState({
        value: formattedNumber,
      });
    }

    render() {
      const { value } = this.state;

      return (
        <input
          value={value}
          className={s.inputValue}
          onChange={this.handleChange}
          inputMode="numeric"
          maxLength={10}
        />
      );
    }
}

export default Input;

Input.propTypes = {
  value: pt.string, // eslint-disable-line 
  onChange: pt.func,
};

Input.defaultProps = {
  value: '',
  onChange: noop,
};

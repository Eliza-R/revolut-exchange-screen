import { formatNumber } from '../Input';

it('formatNumber', () => {
  expect(formatNumber('-34,3')).toBe('-34.3');
  expect(formatNumber('-34,')).toBe('-34.');
  expect(formatNumber('-34,,')).toBe('-34.');
  expect(formatNumber('-34,,,')).toBe('-34.');
  expect(formatNumber('-34.,')).toBe('-34.');
  expect(formatNumber('-34.9.')).toBe('-34.9');
  expect(formatNumber('-34.989')).toBe('-34.98');
  expect(formatNumber('-34')).toBe('-34');
  expect(formatNumber('-0')).toBe('-0');
  expect(formatNumber('-0.3')).toBe('-0.3');
  expect(formatNumber('-0.3.')).toBe('-0.3');
  expect(formatNumber('-00')).toBe('-0');
  expect(formatNumber('-1100')).toBe('-1100');
});

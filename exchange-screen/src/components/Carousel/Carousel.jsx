import React, { PureComponent } from 'react';
import pt from 'prop-types';
import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css';

import { noop } from '../../helpers/utils';
import './Carousel.css';

class CarouselWrapper extends PureComponent {
    static propTypes = {
      children: pt.node,
      onChange: pt.func,
      index: pt.number,
    };

    static defaultProps = {
      children: null,
      onChange: noop,
      index: 0,
    };

    handleChange = (index, elem) => {
      const { onChange } = this.props;

      if (elem) {
        onChange(index, elem.props.currencyName);
      }
    };

    render() {
      const { children, index } = this.props;

      return (
        <Carousel
          showThumbs={false}
          showStatus={false}
          onChange={this.handleChange}
          selectedItem={index}
        >
          {children}
        </Carousel>
      );
    }
}

export default CarouselWrapper;

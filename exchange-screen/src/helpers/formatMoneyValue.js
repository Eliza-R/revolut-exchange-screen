import { isValue } from './utils';

export default (sign, value) => {
  if (value === undefined) {
    return undefined;
  }
  const pureValue = value.toString().replace(sign, '');
  return isValue(pureValue) ? `${sign}${pureValue}` : undefined;
};

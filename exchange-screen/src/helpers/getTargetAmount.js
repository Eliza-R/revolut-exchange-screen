import { round2 } from './utils';
// TODO: unsafe - move to backend
const getTargetAmount = (
  sourceValue, sourceCurrency, targetCurrency, rates,
) => {
  const amountInBase = sourceValue / rates[sourceCurrency];
  return round2(amountInBase * rates[targetCurrency]);
};

export default getTargetAmount;

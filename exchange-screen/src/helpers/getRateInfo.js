import { isValue } from './utils';
import getCurrencySign from './getCurrencySign';

const getRateInfo = (sourceCurrencyName, targetCurrencyName, rate) => {
  if (!isValue(rate)) {
    return '';
  }
  return `${getCurrencySign(targetCurrencyName)}1 = ${getCurrencySign(sourceCurrencyName)}${rate}`;
};

export default getRateInfo;

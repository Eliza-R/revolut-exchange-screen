import getTargetAmount from '../getTargetAmount';

it('getTargetAmount calculates target amount correctly', () => {
  expect(getTargetAmount(2, 'GBP', 'EUR', { EUR: 0.877344, GBP: 0.786918 })).toBe(2.23);
  expect(getTargetAmount(1, 'GBP', 'EUR', { EUR: 0.877344, GBP: 0.786918 })).toBe(1.11);
  expect(getTargetAmount(0, 'GBP', 'EUR', { EUR: 0.877344, GBP: 0.786918 })).toBe(0);
  expect(getTargetAmount(0, 'USD', 'EUR', { EUR: 0.877344, GBP: 0.786918, USD: 1 })).toBe(0);
  expect(getTargetAmount(8, 'USD', 'EUR', { EUR: 0.877344, GBP: 0.786918, USD: 1 })).toBe(7.02);
});

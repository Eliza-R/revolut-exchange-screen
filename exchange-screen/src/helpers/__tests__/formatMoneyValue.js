import formatMoneyValue from '../formatMoneyValue';

it('formatMoneyValue', () => {
  expect(formatMoneyValue('+', '0')).toBe('+0');
  expect(formatMoneyValue('+', '')).toBe(undefined);
  expect(formatMoneyValue('-', '1')).toBe('-1');
  expect(formatMoneyValue('-', '-1')).toBe('-1');
  expect(formatMoneyValue('-', '-')).toBe(undefined);
  expect(formatMoneyValue('+', '+')).toBe(undefined);
  expect(formatMoneyValue('+', undefined)).toBe(undefined);
});

import { isValue } from '../utils';

it('isValue', () => {
  expect(isValue(0)).toBe(true);
  expect(isValue('0')).toBe(true);
  expect(isValue('')).toBe(false);
  expect(isValue('1')).toBe(true);
  expect(isValue(1)).toBe(true);
  expect(isValue(undefined)).toBe(false);
  expect(isValue(null)).toBe(false);
  expect(isValue(NaN)).toBe(false);
});

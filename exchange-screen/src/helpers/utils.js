export const noop = () => {};
export const isValue = value => !!value || value === 0;
export const round2 = num => Number(`${Math.round(`${num}e2`)}e-2`);

import { pathOr } from 'ramda';
import currencies from '../constants/currencies';

export default currency => pathOr('', [currency, 'sign'], currencies);

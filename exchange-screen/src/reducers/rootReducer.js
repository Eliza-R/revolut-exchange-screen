import { combineReducers } from 'redux';
import {
  CHANGE_SOURCE,
  UPDATE_RATES,
  CURRENCY_CHANGED,
  ERROR,
  EXCHANGE,
} from '../constants/actions';
import {
  DEFAULT_CURRENCY_FROM,
  DEFAULT_CURRENCY_TO,
} from '../constants/defaults';
import { round2 } from '../helpers/utils';

const initialSourceState = {
  currencyFrom: DEFAULT_CURRENCY_FROM,
  currencyTo: DEFAULT_CURRENCY_TO,
  indexFrom: 0,
  indexTo: 0,
  accounts: {
    USD: 344.4,
    GBP: 111.3,
    EUR: 112.3,
    CHF: 113.3,
    CNY: 111.3,
    COP: 0,
  },
};

const initialRatesState = {
  rates: {},
  ratesLoaded: false,
};

const getCurrenciesState = ({ index, currencyName, isSource }) => {
  const postfix = isSource ? 'From' : 'To';

  return {
    [`currency${postfix}`]: currencyName,
    [`currency${postfix}`]: currencyName,
    [`index${postfix}`]: index,
    [`index${postfix}`]: index,
  };
};

const source = (state = initialSourceState, action) => {
  const { payload } = action;

  switch (action.type) {
    case CHANGE_SOURCE:
      return {
        ...state,
        sourceValue: payload,
      };
    case CURRENCY_CHANGED:
      return {
        ...state,
        ...getCurrenciesState(payload),
      };
    case EXCHANGE:
      return {
        ...state,
        accounts: {
          ...state.accounts,
          [payload.currencyFrom]: round2(state.accounts[payload.currencyFrom] - Number(payload.sourceValue)),
          [payload.currencyTo]: round2(state.accounts[payload.currencyTo] + Number(payload.targetValue)),
        },
      };
    default:
      return state;
  }
};

const rates = (state = initialRatesState, action) => {
  switch (action.type) {
    case UPDATE_RATES:
      return {
        base: action.base,
        rates: action.rates,
        ratesLoaded: true,
      };
    case ERROR:
      return {
        ...initialRatesState,
        error: true,
      };
    default:
      return state;
  }
};

export default combineReducers({
  source,
  rates,
});

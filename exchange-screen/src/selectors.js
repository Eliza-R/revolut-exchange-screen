import memoize from 'memoize-one';

const getEssentialRates = memoize((currencyFrom, currencyTo, base, baseRate, toRate, fromRate) => (
  { [base]: baseRate, [currencyFrom]: fromRate, [currencyTo]: toRate }));

export const getRates = ( //eslint-disable-line
  { currencyFrom, currencyTo } = {}, rates, base,
) => getEssentialRates(currencyFrom, currencyTo, base, rates[base], rates[currencyTo], rates[currencyFrom]);

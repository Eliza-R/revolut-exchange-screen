import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import 'normalize.css';
import s from './App.module.css';
import ExchangeModule from './components/ExchangeModule/ExchangeModule';
import changeSourceAction from './actions/changeSource';
import updateRatesAction from './actions/rates';
import changeCurrencyAction from './actions/changeCurrency';
import exchangeAction from './actions/exchange';
import { getRates } from './selectors';

const mapStateToProps = ({ source, rates: { rates, base } = {}, rates: ratesState }) => ({
  ...source,
  ...ratesState,
  rates: getRates(source, rates, base),
});

const mapDispatchToProps = dispatch => ({
  changeSource: value => dispatch(changeSourceAction(value)),
  updateRates: value => dispatch(updateRatesAction(value)),
  changeCurrency: (...args) => dispatch(changeCurrencyAction(...args)),
  exchange: payload => dispatch(exchangeAction(payload)),
});

class App extends PureComponent {
  componentDidMount() {
    const { updateRates } = this.props;
    updateRates();
  }

  render() {
    const { updateRates, error, ...rest } = this.props;
    if (error) {
      return <div className={s.error}>Error. Try later</div>;
    }
    return (
      <div className={s.app}>
        <ExchangeModule {...rest} />
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);

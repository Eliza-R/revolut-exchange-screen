import {
  EXCHANGE,
} from '../constants/actions';

const changeSource = payload => (dispatch) => {
  dispatch({
    type: EXCHANGE,
    payload,
  });
};

export default changeSource;

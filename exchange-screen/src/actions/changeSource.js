import {
  CHANGE_SOURCE,
} from '../constants/actions';

const changeSource = value => (dispatch) => {
  dispatch({
    type: CHANGE_SOURCE,
    payload: value,
  });
};

export default changeSource;

import {
  UPDATE_RATES,
  ERROR,
} from '../constants/actions';

const LATEST_RATES_URL = 'https://openexchangerates.org/api/latest.json?app_id=0ad1a75c8a2b4c5fbd31c4964ed57acf';
const SECOND = 1000;
const updateRates = dispatch => () => fetch(LATEST_RATES_URL)
  .then(result => result.json())
  .then(payload => dispatch({
    type: UPDATE_RATES,
    ...payload,
  }))
  .catch(() => dispatch({
    type: ERROR,
  }));


const fetchRates = () => (dispatch) => {
  const fetchFn = updateRates(dispatch);

  return fetchFn().then(() => setInterval(fetchFn, 10 * SECOND));
};


export default fetchRates;

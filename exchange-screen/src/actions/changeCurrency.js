import {
  CURRENCY_CHANGED,
} from '../constants/actions';

const changeCurrency = (index, currencyName, isSource) => (dispatch) => {
  dispatch({
    type: CURRENCY_CHANGED,
    payload: { currencyName, index, isSource },
  });
};

export default changeCurrency;
